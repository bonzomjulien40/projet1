#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <time.h>


void generate_password(int longueur,int speciaux);
int is_password_strong(char chaine[100]);
void cypher_rotate(char chaine[100], int sensRotation, int decalage);
void get_file_header(char fileName[]);

int main(int argc, char *argv[])
{
    int output;
    char mot[100]="zzzzz";
    //generate_password(10,0);

    scanf("%s", mot);
    //output = is_password_strong(mot);
    //printf("%d",output);

    //mot="ab2aZ";

    cypher_rotate(mot,1,2);
}



void generate_password(int longueur,int speciaux)
{
    int i;
    int chiffreAsciAleatoire;
    char motDePasse[longueur];
    int passage;

    srand( time( NULL ) );
    if (speciaux==0)
    {
        
        for ( i = 0; i < longueur; i++)
        {
            
            chiffreAsciAleatoire = rand() % 93 + 33;
            motDePasse[i]=chiffreAsciAleatoire;
            
        }
    }
    else
    {
        for (i = 0; i < longueur; i++)
        {
            
            passage=0;
            while (passage<65 || passage>122 || (passage<97 && passage>90))
            {
                
               passage = rand() % 93 + 33;
            }
            chiffreAsciAleatoire = passage;
            motDePasse[i]=chiffreAsciAleatoire;
            
            
        }


    }





}

int is_password_strong(char chaine[100])
{
    int longBool=0;
    int majusculeBool=0;
    int chiffreBool=0;
    int specialBool=0;
    int longueur = strlen(chaine);
    

    if (longueur >= 10)
        {
        longBool=1;
        }

    for (int i = 0; i < longueur; i++)
    {
        if (chaine[i]>64 && chaine[i]<91)
        {
            majusculeBool=1;
            break;
        }
        
    }



    for (int j = 0; j < longueur; j++)
    {
        if (chaine[j]>47 && chaine[j]<58)
        {
            chiffreBool=1;
            break;
        }
        
    }



    for (int k = 0; k < longueur; k++)
    {
        if ((chaine[k]>32 && chaine[k]<48)||(chaine[k]>57 && chaine[k]<64)||(chaine[k]>90 && chaine[k]<97)||(chaine[k]>122 && chaine[k]<127))
        {
            specialBool=1;
            break;
        }
    }



    if ((longBool==1) && (majusculeBool==1) && (chiffreBool==1) && (specialBool==1))
    {
        return 1;
    }
    else
    {
        return 0;
    }
    


}

void cypher_rotate(char chaine[100], int sensRotation, int decalage)
{

    int longueur = strlen(chaine);
    int mouvement;
    if (sensRotation==0)
    {
        mouvement= decalage;
    }
    else
    {
        mouvement= - decalage;

    }    

    for (int i = 0; i < longueur; i++)
    {
        if ((chaine[i]>96) && (chaine[i]<123))
        {
            
            
            if ((chaine[i] + (mouvement)) >= 123)
            { 
                int excedent=chaine[i]+mouvement-122;
                 
                chaine[i] = 96 +  excedent;
            }
            else if ((chaine[i] + (mouvement)) <= 96)
            {
                int excedent = 97-(chaine[i]+mouvement);
                chaine[i] = 123 -  excedent;
            }
            
            else
            {
                chaine[i] =chaine[i]+mouvement;
            }

        }
               
    }

    puts(chaine);
    


}

void get_file_header(char fileName[])
{
FILE *fptr;
    fptr = fopen(fileName, "r");

    char data[100];

    fgets(data, 8, fptr);



    
    
    
    
    
    
    
    fclose(fptr); // "libère" le fichier

    printf("%s\n", data);


}
